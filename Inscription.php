<?php

$alert = array();

session_start();

$alert['EMPTY'] = '';

if (isset($_POST['nom'])) {
  if (empty($_POST['nom'])) {
    $alert['EMPTY'] = $alert['EMPTY'] . "Veuillez saisir votre nom.<br>";
  }
  if (empty($_POST['prenom'])) {
    $alert['EMPTY'] = $alert['EMPTY'] . "Veuillez saisir votre prénom.<br>";
  }
  if (empty($_POST['adresseMail'])) {
    $alert['EMPTY'] = $alert['EMPTY'] . "Veuillez saisir une adresse mail.<br>";
  }
  if (empty($_POST['password1'])) {
    $alert['EMPTY'] = $alert['EMPTY'] . "Veuillez saisir un mot de passe.<br>";
  }
  if (empty($_POST['password2'])) {
    $alert['EMPTY'] = $alert['EMPTY'] . "Veuillez confirmer votre mot de passe.";
  }
  if ((!empty($_POST['password1']) && !empty($_POST['password1'])) && ($_POST['password1'] != $_POST['password2'])) {
    $alert['DIFF_MDP'] = "Les deux mot de passe ne correspondent pas.";
  } else {

    try {
      $bdd = new PDO('mysql:host=localhost;dbname=bdd_app_web_ping;charset=utf8', 'root', 'root');
    } catch (Exception $e) {
      die('Erreur : ' . $e->getMessage());
    }

    include('connexionBDD.php');


    if (!empty($donnes['adresseMail'])) {
      $alert['MAIL_USED'] = "Cette adresse mail est déja utilisée";
    } else {
      $req = $bdd->prepare('INSERT INTO utilisateurs(nom, prenom, adresseMail, password) VALUES(:nom, :prenom, :adresseMail, :password)');
      $req->execute(array(
        'nom' => $_POST['nom'],
        'prenom' => $_POST['prenom'],
        'adresseMail' => $_POST['adresseMail'],
        'password' => password_hash($_POST['password1'], PASSWORD_DEFAULT)
      ));

      $alert['Inscription'] = "Votre inscription a bien été enregistrée !";

      $req->closeCursor();
    }
  }
}
?>


<!doctype html>
<html lang="fr">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="style.css" />

  <title>Inscription</title>
</head>

<body>

  <?php include('navbar.php');


  if (!empty($alert['EMPTY'])) {
    ?>
    <div class="alert alert-danger alert-dismissible fade show mt-1" role="alert">
      <?php echo $alert['EMPTY'] ?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  <?php
  }


  if (!empty($alert['Inscription'])) {
    ?>
    <div class="alert alert-success alert-dismissible fade show mt-1" role="alert">
      <?php echo $alert['Inscription'] ?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  <?php
  }
  ?>

  <div class="container">
    <div class="col-md-6 border mx-auto my-3 p-4">
      <h3> Inscription </h3>
      <form method="post" action="Inscription.php">
        <div class="form-row">
          <div class="col-md-6 mb-12">
            <label for="validationDefault01">Prénom</label>
            <input type="text" class="form-control mb-3" id="validationDefault01" placeholder="Ex : Swann" name="prenom" required>
          </div>
          <div class="col-md-6 mb-12">
            <label for="validationDefault01">Nom</label>
            <input type="text" class="form-control mb-3" id="validationDefault01" placeholder="Ex : Le Pallec" name="nom" required>
          </div>
        </div>

        <div class="form-row">
          <div class="col-md-12">
            <label for="validationDefault01">Email</label>
            <input type="email" class="form-control mb-3" id="inputEmail3" placeholder="Ex : swann.lepallec@groupe-esigelec.org" name="adresseMail" required>
          </div>
        </div>
        <?php
        if (isset($alert['MAIL_USED'])) {
          ?>
          <div class="alert alert-danger alert-dismissible fade show mt-1" role="alert">
            <?php echo $alert['MAIL_USED'] ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        <?php
        }
        ?>

        <div class="form-row">
          <div class="col-md-12">
            <label for="validationDefault01">Mot de passe</label>
            <input type="password" class="form-control mb-3" id="inputPassword3" placeholder="mdp" name="password1" required>
          </div>
        </div>

        <div class="form-row">
          <div class="col-md-12">
            <label for="validationDefault01">Confirmation du mot de passe</label>
            <input type="password" class="form-control mb-3" id="inputPassword3" placeholder="" name="password2" required>
            <?php
            if (isset($alert['DIFF_MDP'])) {
              ?>
              <div class="alert alert-danger alert-dismissible fade show mt-1" role="alert">
                <?php echo $alert['DIFF_MDP'] ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php
            }
            ?>
          </div>
        </div>

        <button class="btn btn-info mt-4" type="submit">S'inscrire</button>
      </form>
    </div>
  </div>
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

<?php include('footer.php'); ?>

</html>