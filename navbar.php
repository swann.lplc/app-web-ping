<img src="images/panoramaESIG.jpg" class="img-fluid" alt="">
<nav class="navbar navbar-expand-lg navbar-light navbar bg-info sticky-top mb-5">
  <a class="navbar-brand" href="#">
    <img src="images/logoEsigelec.png" width="auto" height="40" class="d-inline-block" alt="">
    <img src="images/PING.png" width="auto" height="35" class="d-inline-block align-top" alt="" disable></a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link text-white" href="index.php">
          <h5>Accueil</h5><span class="sr-only">(current)</span>
        </a>
      </li>

      <?php
      if (isset($_SESSION['nom'])) {
        ?>
        <li class="nav-item">
          <a class="nav-link text-white" href="monEspace.php">
            <h5>Mon Espace</h5>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link text-white" href="voirProjets.php">
            <h5>Les projets</h5>
          </a>
        </li>



    </ul>
    <div>
      <a class="nav-link text-dark" href="index.php?deco=1">
        <h5>Déconnexion</h5>
      </a>

    <?php
    } else {
      ?>
      </ul>
      <a class="nav-link text-white" href="inscription.php">
        <h5>Inscription</h5>
      </a>

    <?php
    }
    ?>
    </div>
  </div>
</nav>