<!-- questions profs :
  - Est ce que une election ne peut être géré que par un admin ?
  - Est ce que on peut lancer plusieurs elections en même temps ?
  - Est ce qu'on garde en mémoire les élections ?
  - Est ce possible de coder le php de manière moins bordélique ?
-->

<?php

session_start();

if (isset($_GET["deco"]) && $_GET["deco"] == 1) {
	session_destroy();
	header('Location: index.php'); //on reactualise la page sinon le session_destroy n'est pas pris en compte
}

if (isset($_POST["adresseMail"])) {

	include('connexionBDD.php');
	//Attention, on utilise 'prepare' et 'execute' a la place de 'query'
	$req = $bdd->prepare('SELECT * FROM utilisateurs WHERE adresseMail = ?');
	$req->execute(array($_POST['adresseMail']));


	$donnes = $req->fetch(); //permet de lire une ligne

	if (password_verify($_POST['password'], $donnes['password'])) {

		$_SESSION['nom'] = $donnes['nom'];
		$_SESSION['prenom'] = $donnes['prenom'];
		$_SESSION['adresseMail'] = $donnes['adresseMail'];
		$_SESSION['password'] = $donnes['password'];
		$_SESSION['id'] = $donnes['id'];
		$_SESSION['isAdmin'] = $donnes['isAdmin'];
		$_SESSION['vote'] = $donnes['vote'];

		header('Location: monEspace.php');
		exit();
	} else {
		$pasConnecte = 1;
	}
	$req->closeCursor();
}


?>


<!doctype html>
<html lang="fr">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="style.css" />

	<title>Projet Ping</title>
</head>

<body>

	<?php include('navbar.php'); ?>

	<!-- Ceci est un commentaire de test -->

	<div class="container">


		<div class="row">
			<div class="col-md-6 border mx-auto mb-3 p-4">
				<h6>Le projet Ping, qu'est ce que c'est ?</h6>
				<div>
					Le Projet INGénieur de l'ESIGELEC est le projet réalisé par les étudiants de dernière année de l'école. Il est l'occasion pour eux d'éprouver leur qualités d'innovation et de conception, tout en expérimentant le travail en équipe. Sur ce site nous vous proposons de découvrir leur réalisations et de voter pour celle que vous estimez la meilleure.
				</div>
			</div>
		</div>

		<?php
		if (!isset($_SESSION["nom"])) {
			?>

			<div class="row">

				<div class="col-md-6 border mx-auto mt-3 p-4">
					<form method="post" action="index.php">
						<div class="form-group">
							<label for="exampleInputEmail1">Login</label>
							<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="adresseMail" required>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Mot de passe</label>
							<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" required>
						</div>
						<?php
							if (isset($pasConnecte) && $pasConnecte == 1) {
								$pasConnecte = 0;
								?>
							<div class="alert alert-danger alert-dismissible fade show mt-1" role="alert">
								Adresse mail ou mot de passe incorrect.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
						<?php
							}
							?>

						<button type="submit" class="btn btn-primary">Valider</button>
					</form>



				</div>

			</div>
		<?php
		}
		?>


	</div>


	<?php include('footer.php'); ?>





	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>