<?php
session_start();

$vote;

if(!isset($_SESSION['nom'])){
    echo "<h1>Veuillez vous connectez pour afficher cette page. </h1>";
    echo "<a href='index.php'>Accueil</a>";
  }
  else{
    include('connexionBDD.php');

    if($_SESSION['vote'] == null && isset($_POST['vote'])){
      $req = $bdd->prepare('UPDATE utilisateurs SET vote = ? WHERE id = ?');
      $req->execute(array($_POST['vote'], $_SESSION['id']));
      $req->closeCursor();

      $_SESSION['vote'] = $_POST['vote'];

      $vote = "Votre vote à bien été comptabilisé !";
    }
?>
<!doctype html>
<html lang="fr">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="style.css" />

  <title>Les Projets</title>
</head>

<body style="background-color:#e3f2fd;">

  <?php include('navbar.php'); ?>

    <div class ='container'>
      <?php
    if (!empty($vote)) {
    ?>
    <div class="alert alert-success alert-dismissible fade show mt-1" role="alert">
      <?php echo $vote ?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  <?php
  }
  ?>
  </div>

  <div class="container_fluid mx-auto mt-4" style="height: auto;">
  
    <div class="col-md-10 mb-11 mx-auto border">

      <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
       
          <?php 
              $req = $bdd->query('SELECT sujets.id, id_election, titre_sujet, description_sujet, url_poster FROM sujets INNER JOIN elections ON sujets.id_election = elections.id WHERE elections.statut = 1 || elections.statut = 2');
                $posters = $req->fetch();
          ?>

          <div class="carousel-item active">
            <div class="row">
              <div class="col-12 text-center mb-3">
               <h3><?php echo $posters['titre_sujet'] ?></h3>
               <?php echo $posters['description_sujet'] ?>
             </div>
            </div>
            <div class="row">
             <div class="col-md-6 mb-11 mx-auto">
                <img src="<?php echo $posters['url_poster'] ?>" class="d-block w-100" alt="...">
              </div>
            </div>
            <form class="row" method="post" action="voirProjets.php"> 
              <input type="hidden" name="vote" value="<?php echo $posters['id'] ?>" />
              <button class="btn btn-info mx-auto my-4" type="submit"
              <?php
              if(!empty($_SESSION['vote'])){
                echo "disabled";
              }
              ?>
              >
            Voter</button>
            </form>
          </div>

          <?php
              while($posters = $req->fetch()){
          ?>

          <div class="carousel-item">
            <div class="row">
              <div class="col-12 text-center mb-3">
               <h3><?php echo $posters['titre_sujet'] ?></h3>
               <?php echo $posters['description_sujet'] ?>
             </div>
            </div>
            <div class="row">
             <div class="col-md-6 mb-11 mx-auto">
                <img src="<?php echo $posters['url_poster'] ?>" class="d-block w-100" alt="...">
              </div>
            </div>
            
            <form class="row" method="post" action="voirProjets.php"> 
              <input type="hidden" name="vote" value="<?php echo $posters['id'] ?>" />
              <button class="btn btn-info mx-auto my-4" type="submit"
              <?php
              if(!empty($_SESSION['vote'])){
                echo "disabled";
              }
              ?>
              >
            Voter</button>
            </form>
          </div>

          <?php
          }
          $req->closeCursor();
          ?>

      </div>



      <a class="carousel-control-prev bg-info my-auto rounded-pill" style="height: 200px" href="#carouselExampleFade" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next bg-info my-auto rounded-pill" style="height: 200px" href="#carouselExampleFade" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
</div>














  <?php include('footer.php'); ?>


  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>

<?php
}
?>