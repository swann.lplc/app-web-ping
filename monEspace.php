<!-- Election :
     1 -> election en cours (une seule à la fois)
     2 -> election terminée (on en affichera les résultats)
     3 -> election archivée (signifie qu'on en a donc relancer une)
-->

<?php
session_start();
if (!isset($_SESSION['nom'])) {
  echo "<h1>Veuillez vous connectez pour afficher cette page. </h1>";
  echo "<a href='index.php'>Accueil</a>";
} else {

  //On se connecte à la BDD au début car on fait beacoup d'appels à celle-ci dans le code
  include('connexionBDD.php');


  if (isset($_POST['btn_demarrer'])) {

    //Archive la dernière election si il y en a une
    $req = $bdd->exec('UPDATE elections SET statut = 3 WHERE statut = 2');

    //Crée la nouvelle election
    $req = $bdd->exec('INSERT INTO elections(statut, date_creation) VALUES(1, NOW())');

    //Supprime les anciens votes
    $req = $bdd->exec('UPDATE utilisateurs SET vote = NULL WHERE vote IS NOT NULL');
    $_SESSION['vote'] = NULL;


    $alert = "Election démarrée <br>";
  }
  if (isset($_POST['btn_supprimer'])) {
    $alert = "Election stoppée <br>";

    $req = $bdd->exec('UPDATE elections SET statut = 2 WHERE statut = 1');
  }


  //on récupère l'election en cours car on en a besoin plus tard
  $req = $bdd->query('SELECT id, statut, DATE_FORMAT(date_creation, "le %e/%m/%y à %kh%m") AS date_creation FROM elections WHERE statut = 1 OR statut = 2');
  $election = $req->fetch();
  $req->closeCursor();


  ?>

  <!doctype html>
  <html lang="fr">

  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css" />

    <title>Mon Espace</title>
  </head>

  <body>
    <?php include('navbar.php'); ?>

    <div class="container">

      <?php
        if (!empty($alert)) {
          ?>
        <div class="alert alert-success alert-dismissible fade show mt-1" role="alert">
          <?php echo $alert ?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      <?php
        }
      ?>

      <div class="row">

        <div class="col-md-5 border mx-auto mb-5 p-4">

          <h3> Profile </h3>
          <div class="row">
            <div class="col-md-6 mb-12">
              <h5>Prénom :</h5>
              <p><?php echo $_SESSION['prenom'] ?></p>
            </div>
            <div class="col-md-6 mb-12">
              <h5>Nom :</h5>
              <p><?php echo $_SESSION['nom'] ?></p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <h5>Email :</h5>
              <p><?php echo $_SESSION['adresseMail'] ?></p>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <h5>Vote(s) :</h5>
              <?php
                if ($_SESSION['vote'] == NULL) {
                  echo "Vous n'avez pas encore voté !";
                } else {

                  $req = $bdd->prepare('SELECT titre_sujet FROM utilisateurs INNER JOIN sujets ON utilisateurs.vote = sujets.id WHERE utilisateurs.id = ?');
                  $req->execute(array($_SESSION['id']));
                  $donnes = $req->fetch();
                  $req->closeCursor();

                  echo "Vous avez voté pour le projet : <br>" . $donnes['titre_sujet'] . "<br>";
                }
                ?>

            </div>
          </div>
        </div>


        <div class="col-md-5 border mx-auto mb-5 p-4">
          <h3>Resultat élection</h3>
          <?php
            if (empty($election)) {
              echo "<p>Il n'y a pas d'élection pour le moment. </p>";
            } else if ($election['statut'] == 1) {
              echo "<p>L'élection est encore en cours. Vous aurez accès aux résultats dès que l'administrateur fermera celle-ci. </p>";
            } else if ($election['statut'] == 2) {

              $req = $bdd->query('SELECT COUNT(*) AS nbr_sujets, (SELECT COUNT(*) FROM utilisateurs WHERE vote IS NOT NULL) AS nbr_votes FROM sujets INNER JOIN elections ON sujets.id_election = elections.id WHERE elections.statut = 2');
              $donnees = $req->fetch();
              $req->closeCursor();

              $req = $bdd->query('SELECT titre_sujet, sujets.id AS id_sujets, (SELECT COUNT(*) FROM utilisateurs INNER JOIN sujets ON utilisateurs.vote = sujets.id WHERE sujets.id = id_sujets) AS nbr_votes FROM sujets INNER JOIN elections ON sujets.id_election = elections.id WHERE elections.statut = 2 ORDER BY nbr_votes DESC');

              echo "L'élection a commencé " . $election['date_creation'] . "<br>";
              echo "Il y avait " . $donnees['nbr_sujets'] . " sujets pour " . $donnees['nbr_votes'] . " votants. <br><br>";
              $resultats = $req->fetch();
              echo "1ère place : <span class=\"font-weight-bold\">" . $resultats['titre_sujet'] . "</span> avec " . $resultats['nbr_votes'] . " votes.<br><br>";
              $resultats = $req->fetch();
              echo "2ème place : <span class=\"font-weight-bold\">" . $resultats['titre_sujet'] . "</span> avec " . $resultats['nbr_votes'] . " votes.<br><br>";
              $resultats = $req->fetch();
              echo "3éme place : <span class=\"font-weight-bold\">" . $resultats['titre_sujet'] . "</span> avec " . $resultats['nbr_votes'] . " votes.<br><br>";
              $req->closeCursor();
            }
            ?>
        </div>


        <!-- partie administrateur -->

        <?php
          if ($_SESSION['isAdmin'] == 1) {
            ?>


          <div class="col-md-5 border mx-auto mb-5 p-4">
            <h3>Gestion de l'élection</h3>

            <h5>Date de début :</h5>
            <?php
                if ($election['statut'] == 1 || 2) {
                  echo "<p>" . $election['date_creation'] . "</p>";
                }
                ?>

            <h5>Nombres de sujets :</h5>
            <?php
                if ($election['statut'] == 1 || 2) {
                  $req = $bdd->query('SELECT COUNT(*) AS nbr_sujets FROM sujets INNER JOIN elections ON sujets.id_election = elections.id WHERE elections.statut = 1');
                  $donnees = $req->fetch();
                  echo "<p>" . $donnees['nbr_sujets'] . "</p>";
                  $req->closeCursor();
                }
                ?>



            <h5>Statut de l'élection :</h5>
            <br>
            <form method="post" action="monEspace.php" class="row">
              <button class="btn btn-info mx-auto" type="submit" name="btn_demarrer" <?php
                                                                                          if ($election['statut'] == 1)
                                                                                            echo "disabled";
                                                                                          ?>>
                Demarrer une élection</button>

              <button class="btn btn-info mx-auto" type="submit" name="btn_supprimer" <?php
                                                                                          if (empty($election) || $election['statut'] == 2)
                                                                                            echo "disabled";
                                                                                          ?>>Stopper l'élection</button>
            </form>

          </div>


          <div class="col-md-5 border mx-auto mb-5 p-4">
            <h3>Liste des sujets</h3>

            <ul class="list-group">

              <p>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                  Nombre de votes
                  <span class="badge badge-info badge-pill">
                    <?php

                        $req = $bdd->query('SELECT COUNT(*) AS nbr_votes FROM utilisateurs WHERE vote IS NOT NULL');
                        $donnees = $req->fetch();
                        $req->closeCursor();

                        echo $donnees['nbr_votes'];
                        ?>
                  </span>
                </li>
              </p>

              <?php
                  $req = $bdd->query('SELECT titre_sujet, sujets.id AS id_sujets, (SELECT COUNT(*) FROM utilisateurs INNER JOIN sujets ON utilisateurs.vote = sujets.id WHERE sujets.id = id_sujets) AS nbr_vote FROM sujets INNER JOIN elections ON sujets.id_election = elections.id WHERE elections.statut = 1 ORDER BY nbr_vote DESC');



                  while ($donnees = $req->fetch()) {

                    echo "<li class=\"list-group-item d-flex justify-content-between align-items-center\">" . $donnees['titre_sujet'] . "
              <span class=\"badge badge-info badge-pill\">" . $donnees['nbr_vote'] . "</span>
              </li>
                ";
                  }
                  $req->closeCursor();
                  ?>
              <br>
              <?php
                if ($election['statut'] == 1) {
                ?>
                  <a href="ajouterSujet.php"> Ajouter un sujet </a>
                <?php
                  }
              ?>
          </div>

        <?php
          }
          ?>


        <!-- Fin partie administrateur -->


      </div>

    </div>



    <?php include('footer.php'); ?>






    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>

  </html>

<?php
}
?>