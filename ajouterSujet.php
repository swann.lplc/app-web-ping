<?php
session_start();

$alert[] = array();
include('connexionBDD.php');

$req = $bdd->query('SELECT id FROM elections WHERE statut = 1');
$election = $req->fetch();
$req->closeCursor();

if (!isset($_SESSION['nom'])) {
  echo "<h1>Veuillez vous connectez pour afficher cette page. </h1>";
  echo "<a href='index.php'>Accueil</a>";
} else if ($_SESSION['isAdmin'] != 1) {
  echo "<h3>Vous n'avez pas les privilèges suffisants pour accéder à cette page.</h3> <br>";
  echo "<a href='index.php'>Accueil</a>";
} else if (empty($election)) {
  echo "Vous ne pouvez pas encore ajouter de sujets ! <br>";
  echo "<a href='index.php'>Accueil</a>";
} else {

  if (isset($_POST["titre_sujet"])) {

    $req = $bdd->prepare('SELECT titre_sujet FROM sujets WHERE titre_sujet = ?');
    $req->execute(array($_POST['titre_sujet']));
    $donnes = $req->fetch();
    $req->closeCursor();

    if (!empty($donnes['titre_sujet'])) {
      $alert['TITRE'] = "Ce titre est déja utilisé !";
    } else {

      // Testons si le fichier a bien été envoyé et s'il n'y a pas d'erreur
      if (isset($_FILES['monFichier']) and $_FILES['monFichier']['error'] == 0) {

        // Testons si le fichier n'est pas trop gros
        if ($_FILES['monFichier']['size'] <= 1000000) {
          // Testons si l'extension est autorisée
          $infosfichier = pathinfo($_FILES['monFichier']['name']);
          $extension_upload = $infosfichier['extension'];
          $extensions_autorisees = array('jpg', 'jpeg', 'gif', 'png');
          $url_poster =  'posters/' . basename($_FILES['monFichier']['name']);

          if (in_array($extension_upload, $extensions_autorisees)) {
            // On peut valider le fichier et le stocker définitivement
            move_uploaded_file($_FILES['monFichier']['tmp_name'], $url_poster);

            $req = $bdd->prepare('INSERT INTO sujets(id_election,titre_sujet, description_sujet, url_poster) VALUES((SELECT id FROM elections WHERE statut = 1), :titre_sujet, :description_sujet, :url_poster)');
            $req->execute(array(
              'titre_sujet' => $_POST['titre_sujet'],
              'description_sujet' => $_POST['description_sujet'],
              'url_poster' => $url_poster
            ));

            $alert['SCS'] = "Nouveau poster ajouté !";

            $req->closeCursor();
          } else {
            $alert['EXT'] = "Erreur ! L'extension est mauvaise (uniquement acceptés : jpg, jpeg, gif, png).";
          }
        } else {
          $alert['VOL'] = "Le fichier est trop volumineux ! (doit être < 1Mo) <br>";
        }
      } else {
        $alert['UPL'] = "Erreur d'upload !";
      }
    }
  }

  ?>

  <!doctype html>
  <html lang="fr">

  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css" />

    <title>Nouveau Sujet</title>
  </head>

  <body>
    <?php include('navbar.php'); ?>

    <div class="container">
      <?php
        if (!empty($alert['UPL'])) {
          ?>
        <div class="alert alert-danger alert-dismissible fade show mt-1" role="alert">
          <?php echo $alert['UPL'] ?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      <?php
        }
        if (!empty($alert['SCS'])) {
          ?>
        <div class="alert alert-success alert-dismissible fade show mt-1" role="alert">
          <?php echo $alert['SCS'] ?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      <?php
        }
        ?>
    </div>
    <div class="container">
      <div class="col-md-6 border mx-auto my-3 p-4">
        <h3> Nouveau Projet </h3>

        <form method="post" action="ajouterSujet.php" enctype="multipart/form-data">

          <label for="validationDefault01">Nom du nouveau projet :</label>
          <input type="text" class="form-control" id="validationDefault01" placeholder="Sujet" name="titre_sujet" required>
          <?php
            if (!empty($alert['TITRE'])) {
              ?>
            <div class="alert alert-danger alert-dismissible fade show mt-1" role="alert">
              <?php echo $alert['TITRE'] ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          <?php
            }
            ?>
          <br>
          <label for="validationDefault01">Description du nouveau projet :</label>
          <input type="text" class="form-control" id="validationDefault01" placeholder="Description" name="description_sujet" required>
          <br>
          <label for="validationDefault01">Poster du projet :</label>
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="validatedCustomFile" name="monFichier" required>
            <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
            <div class="invalid-feedback">Example invalid custom file feedback</div>
          </div>
          <?php
            if (!empty($alert['EXT'])) {
              ?>
            <div class="alert alert-danger alert-dismissible fade show mt-1" role="alert">
              <?php echo $alert['EXT'] ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          <?php
            }
            if (!empty($alert['VOL'])) {
              ?>
            <div class="alert alert-danger alert-dismissible fade show mt-1" role="alert">
              <?php echo $alert['VOL'] ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          <?php
            }
            ?>

          <button class="btn btn-info mt-4" type="submit">Ajouter</button>


        </form>
      </div>
    </div>


    <?php include('footer.php'); ?>






    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>

  </html>

<?php
}
?>